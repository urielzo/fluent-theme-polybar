# fluent-theme-polybar

theme for polybar

## Preview

## fluent
![light](/preview/fluent.png)
<br />
## fluent-dark
![dark](/preview/fluent-dark.png)
<br />
## fluent-green
![green](/preview/fluent-green.png)
<br />


## Fonts

- **iosevka-term**
- **feather**
- **Font Awesome 5 Freze**
- **Weather Icons**

### Other prerequisites for polybar module
* `nitrogen, feh`
* `rofi`
* `i3lock, betterlockscreen`

## Details
- **Distro** ubuntu ;)
- **WM** i3wm
- **Panel** Polybar
- **Terminal** Alacritty
- **Music Player** io.elementary.music,
- **Text Editor** vim
- **File Manager** Ranger, with w3m image previewer
- **Alternative File Manager** Thunar
- **Screen Recorder** simplescreenrecorder
- **Program Launcher** rofi
- **Info Fetcher** Neofetch
- **GTK Theme** Fluent-dark
- **Icons** Fuent-grey-dark
- **CLI Shell Intepreter** fish
- **Compositor** picom
- **Notification Daemon** dunst

## add this line to your ~/.config/i3/config

## exec --no-startup-id ~/.config/polybar/panels/polybar-wrapper.sh &




- ** Special Thanks
- ** adi1090x: [polybar themes](https://github.com/adi1090x/polybar-themes)

