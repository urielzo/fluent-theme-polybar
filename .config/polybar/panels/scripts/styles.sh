#!/usr/bin/env bash

DIR="$HOME/.config/polybar/panels"

change_panel() {
	# replace config with selected panel
	cat "$DIR"/panel/"${panel}.ini" > "$DIR"/config.ini

	# Change wallpaper
	feh --bg-fill "$DIR"/wallpapers/"$bg"
	
	# Restarting polybar
	polybar-msg cmd restart
}

if  [[ "$1" = "--fluent" ]]; then
	panel="fluent"
	bg="fluent-light.png"
	change_panel

elif  [[ "$1" = "--fluent-dark" ]]; then
	panel="fluent-dark"
	bg="fluent-dark.png"
	change_panel
	
elif [[ "$1" = "--fluent-green" ]]; then
	panel="fluent-green"
	bg="fluent-green.png"
	change_panel	

else
	cat <<- _EOF_
	No option specified, Available options:
	--fluent   --fluent-dark --fluent-green
	_EOF_
fi
