#!/usr/bin/env bash

DIR="$HOME/.config/polybar/panels/menu"

launcher() {
	rofi -no-config -no-lazy-grab -show drun -modi drun -theme "$DIR"/"$theme"/launcher.rasi
}

if  [[ "$1" = "--fluent" ]]; then
	theme="fluent"
	launcher

elif  [[ "$1" = "--fluent-dark" ]]; then
	theme="fluent-dark"
	launcher

elif  [[ "$1" = "--fluent-green" ]]; then
	theme="fluent-green"
	launcher	

else
	rofi -e "No theme specified."
	exit 1
fi
