#!/usr/bin/env bash

SDIR="$HOME/.config/polybar/panels/scripts"
DIR="$HOME/.config/polybar/panels/menu"

if  [[ "$1" = "--fluent" ]]; then
	theme="fluent"

elif  [[ "$1" = "--fluent-dark" ]]; then
	theme="fluent-dark"
	
elif  [[ "$1" = "--fluent-green" ]]; then
	theme="fluent-green"	

else
	rofi -e "No theme specified."
	exit 1
fi

# Launch Rofi
MENU="$(rofi -no-config -no-lazy-grab -sep "|" -dmenu -i -p '' \
-theme $DIR/$theme/styles.rasi \
<<< " fluent| fluent-dark| fluent-green|")"
            case "$MENU" in
				*fluent) "$SDIR"/styles.sh --fluent ;;
				*fluent-dark) "$SDIR"/styles.sh --fluent-dark ;;
				*fluent-green) "$SDIR"/styles.sh --fluent-green ;;
            esac
